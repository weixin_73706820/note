# QT IMX6ULL

## 基础篇

### 信号与槽机制

由于通过UI法生成的各种信号需要在编译后生成到对应的代码区，因此比较难以修改与观察，所以一般用代码来实现各种重要的信号槽机制。

#### 信号声明

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-19-56-34-image.png)

#### 槽函数定义

由于槽函数作物处理信号者（可以是任意函数）。其必须是具体的，且需要告知发送者一些响应？

因此，槽函数要求至少两点

1. 必须有具体定义

2. 信号参数与返回值一致。如果发信者为void函数，那就不能返回任何值

声明实现

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-20-03-20-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-20-13-13-image.png)

#### 连接函数

首先需要弄清楚我们当前有的部分

1. 窗体
   
   1. 窗体里面声明的button类对象
   
   2. 窗体里面的其他函数
      
      信号函数：
      
      两个槽函数
      
      ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-20-15-31-image.png)

也因此，现在，我们希望实现的是，点击按钮，能让按钮内的字体发生变化，我们的想法实现路径就是：

button对象内在的click()检测函数被激活 发信号-->对象所在窗体的对象承接函数（pushButtonClick（）)接受信号。--》承接函数调用信号函数发出信号 pushButtonTestChange（）--->相应的槽函数接收到改变内容的信号，改变内容ChangeButtonTest.

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-20-24-07-image.png)

定义发生在cpp文件内

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-20-26-15-image.png)

**简单来说，connect的重点在于理清要收发的函数，以及持有他们的对象**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-20-33-53-image.png)

一个奇特的发现是，如果没有connect，直接用emit也可以触发pushButtonClick()函数的触发。

### 其他的对象

#### 各种button

pushbutton；

**ToolButton**

第一步， 引入标准类头文件

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-21-21-55-image.png)

第二步， 实现定义

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-21-33-43-image.png)

结果就是这样

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-21-36-59-image.png)

**radioBT 关于qss样式表**

1. 生成无ui项目， 添加资源文件。
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-16-34-42-image.png)
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-16-35-07-image.png)

2. 添加资源文件管理的前缀
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-16-38-19-image.png)

3. 添加资源文件
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-16-45-41-image.png)

4. 添加qss样式文件
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-16-46-44-image.png)
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-16-47-24-image.png)
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-16-47-42-image.png)

到目前位置，我们做了几件事

<mark>创建资源文件管理器--》载入资源文件夹（前缀设置）--->加入文件</mark>

接下来我们需要调用文件

1. 在mainwindow头文件声明对象
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-16-50-51-image.png)

2. 在mainWindow.cpp文件中实现
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-17-03-39-image.png)

3. 在main.cpp中完成qss设置于导入
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-17-22-42-image.png)

4. 设置样式表
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-17-38-01-image.png)

有趣的地方在于，这个互斥是如何实现的。难道是在声明的时候，会自动排斥吗。

**checkBox**

方法是类似的

1. 添加创建项目

2. 添加资源文件

3. 在main.cpp声明对象
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-17-45-21-image.png)

**LinkButton**

有个特殊的头文件类型

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-22-03-51-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-22-09-39-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-22-09-52-image.png)

**自定义对话框按钮 QDialogButtonBox**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-22-12-51-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-22-16-54-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-22-23-11-image.png)

通过返回的button对比dialogbut中的种类即可。

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-22-27-47-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-29-22-30-49-image.png)

#### 控件

##### QComboBox

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-21-31-05-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-21-38-37-image.png)

###### QFontComboBox

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-21-42-26-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-21-49-41-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-21-48-36-image.png)

##### LineEdit

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-22-07-36-image.png)

想要自主操作某一个控件，关键在于如何选择某个空间。也就是Focus事件。

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-22-21-56-image.png)

##### QPlainTextEdit

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-11-01-13-22-03-image.png)

这里涉及到了路径读取与文件流的问题

想要读取并打开一个文件，尤其是不同文件夹下的时候。需要几个环节。

1. 得到得到文件的路径/或者切换当前的工作目录

2. 通过文件锁定容器，读取文件，并以指定方式打开

3. 开启文件流， 指定读取方式

4. 将文件通过流，放入到指定容器。

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-11-01-13-37-12-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-11-01-13-37-21-image.png)

QPlainTextEdit 可以理解为 QTextEdit 的低配版。 QPlainTextEdit 支持纯文本显示， QTextEdit 支持富文本（支持多种格式，比如插入图片，  链接等）显示。就是多一个样式。 <mark>QPlainTextEdit 显示的效率比 QTextEdit 高，如果需要显示大  量文字，尤其是需要滚动条</mark>

<mark>来回滚动的时候， QPlainTextEdit 要好很多。</mark>

##### QSpinBox

调值按钮

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-11-01-13-54-38-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-11-01-13-54-46-image.png)

##### QDateTime等

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-11-01-14-10-55-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-11-01-14-10-47-image.png)

##### QDial 电位器类似

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-11-01-14-23-59-image.png)

##### QScrollBar

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-11-01-14-30-10-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-11-01-14-36-37-image.png)

这里来看的话，这个滑块的设置还是比较麻烦的在长度上。但是也更加的自由。

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-11-01-14-49-22-image.png)

互相关联的机制

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-11-01-14-50-12-image.png)

##### QKeySequenceEdit

一个新的数据类型：KeySequence；

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-11-01-14-52-30-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-11-01-14-55-24-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-11-01-15-02-18-image.png)



#####  QcalendarWidget



![image-20241104214907586](assets/image-20241104214907586.png)

![image-20241104214950224](assets/image-20241104214950224.png)

![image-20241104214956323](assets/image-20241104214956323.png)

#### ui与视图窗体

![image-20241108201157525](assets/image-20241108201157525.png)

![image-20241108202740748](assets/image-20241108202740748.png)

![image-20241108202826032](assets/image-20241108202826032.png)



#### QGraphicsView

![image-20241108210321301](assets/image-20241108210321301.png)

![image-20241108210336318](assets/image-20241108210336318.png)

![image-20241108210342086](assets/image-20241108210342086.png)

![image-20241108210406203](assets/image-20241108210406203.png)



#### 布局管理

![image-20241111165358246](assets/image-20241111165358246.png)



##### QBoxLayout

![image-20241111170209916](assets/image-20241111170209916.png)

![image-20241111171654010](assets/image-20241111171654010.png)

![image-20241111171517989](assets/image-20241111171517989.png)



##### QGridLayout

![image-20241111172214974](assets/image-20241111172214974.png)

```c++
#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->setGeometry(0, 0, 800, 480);

    gWidget = new QWidget(this);
    this->setCentralWidget(gWidget);            //将之放到中心位置

    gridLayout = new QGridLayout();
    QList <QString> list;

    list <<"button1" << "button2" << "button3" << "button4";
    for(int i=0; i<4; i++){
        pushbutton[i] = new QPushButton();          //数组的独立定义，之前可能接触到比较少
        pushbutton[i]->setText(list[i]);
        pushbutton[i]->setMinimumSize(100, 30);     //最小宽度和高度。。这个可能更多是移植的时候需要着重考虑的
        pushbutton[i]->setSizePolicy(
                        QSizePolicy::Expanding,
                        QSizePolicy::Expanding      //自动调节按钮 宽度和高度，这四个或许可以成为按钮声明的模板
                        );

        switch (i) {                                //奇怪，为什么不用循环？   哦哦，因为行列的问题，不然会覆盖的。
        case 0:
            gridLayout->addWidget(pushbutton[i], 0, 0);
            break;
        case 1:
            gridLayout->addWidget(pushbutton[i], 0, 1);
            break;
        case 2:
            gridLayout->addWidget(pushbutton[i], 1, 0);
            break;
        case 3:
            gridLayout->addWidget(pushbutton[i], 1, 1);
            break;
        default:
            break;
        }
    }
    gridLayout->setRowStretch(0, 2);                //特点，设置第0行的比例系数。比例系数都是行列的一次设计
    gridLayout->setRowStretch(1, 3);

    gridLayout->setColumnStretch(0, 1);
    gridLayout->setColumnStretch(1, 3);

    gWidget->setLayout(gridLayout);
}

MainWindow::~MainWindow()
{
}


```

![image-20241111173517392](assets/image-20241111173517392.png)

从上面来看，还是有很多特别的地方的相较于Box布局。

这个角度看，其实box在某些情况下，可以是GridLayout的特殊形式。

![image-20241111173646998](assets/image-20241111173646998.png)



##### QFormLayout

用于管理输入小部件（如账号密码，文本框啥的）， 多是 （标签：框体） 的结构

会结合以西而接口函数，设置换行（如打字框扩展）；



![image-20241111174244825](assets/image-20241111174244825.png)

![image-20241111175149464](assets/image-20241111175149464.png)

布局管理设计原则

一个布局对应一个widget；

不同的布局需要经可能的实现可移植，适配（例如gridlayout 的比例)

#### 空白间隔管理 QSpacerItem

QSpacerItem 继承 QLayoutItem。 QSpacerItem 类在布局中提供空白(空间间隔)。所以 QSpa
cerItem 是在布局中使用

基本对象声明：

发现，其实类似这种布局的，都需要声明一个外置的容器来装，不然会很麻烦。大概视为

mainWidget--》widget --》mainbox -->box

![image-20241114221140017](assets/image-20241114221140017.png)





![image-20241114222217909](assets/image-20241114222217909.png)

```c++
#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->setGeometry(0, 0, 800, 480);

    widget = new QWidget(this);         //加入当前窗体
    this->setCentralWidget(widget);     //居然不是传地址

    vSpace = new QSpacerItem(10, 10, QSizePolicy::Minimum, QSizePolicy::Expanding);
    hSpace = new QSpacerItem(10, 10, QSizePolicy::Expanding, QSizePolicy::Minimum);     //??

    //实例化
    vBoxLayout = new QVBoxLayout();
    hBoyLayout = new QHBoxLayout();
    mainLayout = new QHBoxLayout();

    //添加间隔
    vBoxLayout->addSpacerItem(vSpace);			//垂直先加入的间隔
    QList <QString>list;
    list<< "button1" << "button2" << "button3"<< "button4";
    for(int i=0; i<4; i++){
        bt[i] = new QPushButton();
        bt[i]->setText(list[i]);
        if(i == 0){
            bt[i]->setFixedSize(100, 100);      //按钮大小
            vBoxLayout->addWidget(bt[i]);       //添加容器对象
        }else{
            bt[i]->setFixedSize(60, 60);
            hBoyLayout->addWidget(bt[i]);
        }
    }
    hBoyLayout->addSpacerItem(hSpace);          //容器添加内容，后计入的
    mainLayout->addLayout(vBoxLayout);          //在主布局添加垂直布局
    mainLayout->addLayout(hBoyLayout);

    mainLayout->setSpacing(50);                 //每个容器的间隔。包括不同layout
    widget->setLayout(mainLayout);              //加入窗体

}

MainWindow::~MainWindow()
{
}


```

奇特的地方在于，这些不同组件怎么知道自己应该从哪里开始。

#### 容器管理

##### QWidget

QWidget 类是所有用户界面对象的基类（如 QLabel 类继承于 QFrame 类，而 QFrame 类又
继承于 QWidget 类）。 ==Widget 是用户界面的基本单元==：它从窗口系统接收鼠标，键盘和其他事
件，并在屏幕上绘制自己。每个 Widget 都是矩形的，它们按照 Z-order 进行排序。

> 注： Z-order 是重叠二维对象的顺序，例如堆叠窗口管理器中的窗口。典型的 GUI 的特征
> 之一是窗口可能重叠，使得一个窗口隐藏另一个窗口的一部分或全部。 当两个窗口重叠时，它
> 们的 Z 顺序确定哪个窗口出现在另一个窗口的顶部。
> 理解：术语"z-order"指沿着 z 轴物体的顺序。三维坐标轴中 x 横轴， y 数轴， z 上下轴。可
> 以将 gui 窗口视为平行与显示平面的一系列平面。因此，窗口沿着 z 轴堆叠。所以 z-order 指定
> 了窗口的前后顺序。就像您桌面上的一叠纸一样，每张纸是一个窗口，桌面是您的屏幕，最上
> 面的窗口 z 值最高。

**==QWidget 不是一个抽象类==**，它可以用作其他 Widget 的容器，并很容易作为子类来创建定制
Widget。它经常用于创建、放置和容纳其他的 Widget 窗口  





##### QGroupBOX

![image-20241116212721201](assets/image-20241116212721201.png)

![image-20241116212705427](assets/image-20241116212705427.png)

##### QScrollArea

滚动视图，结合label看图片

![image-20241116213501974](assets/image-20241116213501974.png)

##### QToolBox  

![image-20241116215750085](assets/image-20241116215750085.png)

```c++
#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->setGeometry(0, 0, 800, 400);

    toolBox = new QToolBox(this);
    toolBox->setGeometry(300, 50, 200, 250);

    toolBox->setStyleSheet("QToolBox {background-color:rgba(0, 0, 0,30%);}");

    for(int i=0; i<2; i++){
        vBoxLayout[i] = new QVBoxLayout();
        groupBox[i] = new QGroupBox(this);
    }

    QList <QString> strList;
    strList<<"李白"<<"王照君"<<"李元芳"<<"程咬金"<<"钟馗"<<"上官婉儿";

    QList <QString>iconsList;
    iconsList<<":/test/libai"<<":/test/wangzhaojun"
    <<":/test/liyuanfang"<<":/test/chengyaojin"
    <<":/test/zhongkui"<<":/test/shangguanwaner";

    for(int i=0; i<6; i++){
        toolButton[i] = new QToolButton;                    //先声明，在用的时候使用的特点
        toolButton[i]->setIcon(QIcon(iconsList[i]));
        toolButton[i]->setText(strList[i]);
        toolButton[i]->setFixedSize(150, 40);
        toolButton[i]->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);        //??

        if(i<3){
            vBoxLayout[0]->addWidget(toolButton[i]);
            vBoxLayout[0]->addStretch(1);               //添加伸缩量
        }else{
            vBoxLayout[1]->addWidget(toolButton[i]);
            vBoxLayout[1]->addStretch(1);
        }
    }

    groupBox[0]->setLayout(vBoxLayout[0]);
    groupBox[1]->setLayout(vBoxLayout[1]);

    toolBox->addItem(groupBox[0], "我的好友");
    toolBox->addItem(groupBox[1], "黑名单");
    
    //toolBox -> groupbox-i-> vboxLayout--i-->toolbutton
}

MainWindow::~MainWindow()
{
}


```





##### QTabWIdget 多页面

![image-20241118215710164](assets/image-20241118215710164.png)

```c++
#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->setGeometry(0, 0, 800, 480);

    widget = new QWidget(this);
    this->setCentralWidget(widget);

    tabWidget = new QTabWidget();

    hBoxLayout = new QHBoxLayout();


    QList<QString>strLabelList;
    strLabelList <<"label 1" << "label 2" << "label 3";

    QList<QString>strTabList;
    strTabList<< "tab 1" << "tab 2" << "tab 3";

    QList<QString>iconList;
    iconList << ":/img/chengyaojin.png" << ":/img/libai.png" << ":/img/liyuanfang.png";

    for(int i=0; i<3; i++){
        label[i] = new QLabel();
        label[i]->setText(strLabelList[i]);
        label[i]->setAlignment(Qt::AlignCenter);        //标签对齐居中
        //添加页面(内容名称， 标签图像路径， 页面栏名称
        tabWidget->addTab(label[i], QIcon(iconList[i]), strTabList[i]);
    }

    tabWidget->setTabsClosable(true);                   //可关闭,这个应该是便于固定在页面上
    hBoxLayout->addWidget(tabWidget);
    widget->setLayout(hBoxLayout);

}

MainWindow::~MainWindow()
{
}


```



##### QStackedWidget栈页面，

特点在于，这个需要通过关联connect来实现页面的切换。



结构如下



mainWidget-->widget-->hBoxLayout-->ListWidget 	| 同索引currentRowChanged(int)

​							         |-->stackWidget    |connect -->setCurrentIndex(int)

以此来实现stackWidget的分层次显示。

![image-20241118222021055](assets/image-20241118222021055.png)

```c++
#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->setGeometry(0, 0, 800, 480);

    widget = new QWidget(this);         //这个的指向倒是特别
    this->setCentralWidget(widget);

    hBoxLayout = new QHBoxLayout();
    stackWidget = new QStackedWidget();


    listWidget = new QListWidget();
    QList <QString>strListWIdgetList;       //?
    strListWIdgetList<< "窗口1" << "窗口2" << "窗口3";

    for(int i=0; i<3; i++){
        //插入项？？
        listWidget->insertItem(i, strListWIdgetList[i]);
    }


    for(int i=0; i<3; i++){
        label[i] = new QLabel();
        label[i]->setText(strListWIdgetList[i]);
        stackWidget->addWidget(label[i]);           //这个栈添加的函数，其参数居然是个str？？
    }

    listWidget->setMaximumWidth(200);                //列表最大宽度
    hBoxLayout->addWidget(listWidget);               //将列表窗口加入布局
    hBoxLayout->addWidget(stackWidget);              //将栈窗口加入布局

    widget->setLayout(hBoxLayout);

    connect(listWidget, SIGNAL(currentRowChanged(int)), stackWidget, SLOT(setCurrentIndex(int)));       //参数配对

}

MainWindow::~MainWindow()
{
}


```



##### QMdiArea 多窗体

级联和平铺。

![image-20241118213018172](assets/image-20241118213018172.png)

```c++
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QMdiSubWindow>
#include <QMdiArea>
#include <QPushButton>
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QMdiArea *mdiArea;      //??区域对象
    QMdiSubWindow *newMdiSubWindow; //??
    QPushButton *pushButton;

private slots:
    void creat_newMdisubWindow();

};
#endif // MAINWINDOW_H

```

```c++
#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->setGeometry(0, 0, 800, 480);
    pushButton = new QPushButton("新建窗口", this);
    pushButton->setGeometry(0, 30, 100, 30);

    mdiArea = new QMdiArea(this);
    mdiArea->setGeometry(100, 30, 700, 430);    //设置区域大小
    connect(pushButton, SIGNAL(clicked()), this, SLOT(creat_newMdisubWindow()));
}

MainWindow::~MainWindow()
{
}

void MainWindow::creat_newMdisubWindow()
{
    newMdiSubWindow = new QMdiSubWindow();
    newMdiSubWindow->setWindowTitle("new window");

    newMdiSubWindow->setAttribute(Qt::WA_DeleteOnClose);        //设置这个属性后，关闭窗口会释放资源，不保留缓存
    mdiArea->addSubWindow(newMdiSubWindow);                     //将窗口放到区域中,自动布局？？

    newMdiSubWindow->show();
    newMdiSubWindow->sizePolicy();                              //?自适应窗口
    mdiArea->tileSubWindows();                                  //平铺排布
//    mdiArea->cascadeSubWindows();                             //级联排列
}


```

特点在于

mdiArea-->MdiSubWindow



##### QDockWidget停靠窗体

