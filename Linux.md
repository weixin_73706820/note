# Linux

## 基本框架：

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-15-24-07-image.png)![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-15-25-31-image.png)

应用-->驱动-->项目

**资料**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-15-30-05-image.png)

### 基本层级结构

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-10-19-19-08-image.png)

### 系统层级结构图

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-10-19-20-42-image.png)

### 配置与准备

日常使用Linux与开发板进行嵌入式开发的和，需要三个东西

开发端   服务器端    嵌入式开发板端

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-15-58-23-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-15-58-30-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-15-58-36-image.png)开发段编写代码编译代码，上传到服务器的ubuntu中

然后烧录板子。

网卡：nat链接外部，作为虚拟机与win主机的中介。

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-16-17-54-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-16-05-03-image.png)

## Linux基本命令

1. ls -a -l -h
2. cd

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-10-20-41-03-image.png)
3. pwd    print work directory
4. mkdir -p(父目录自创建) make direction

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-11-11-28-18-image.png)

5. touch 文件
6. cat 一次性查看

   more 翻页查看
7. cp

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-11-11-33-43-image.png)
8. mv

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-11-11-35-42-image.png)
9. rm

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-11-11-37-14-image.png)

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-11-11-38-51-image.png)
10. which
11. find

    ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-11-18-02-48-image.png)

    ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-11-18-06-12-image.png)
12. grep

    ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-12-16-28-40-image.png)

    ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-12-16-37-45-image.png)

    ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-12-16-38-50-image.png)
13. wc

    ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-12-16-39-17-image.png)
14. | 管道符

    ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-12-16-43-06-image.png)
15. echo

    ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-12-16-52-11-image.png)
16. ` 反引号

    ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-12-16-56-17-image.png)
17. ">   >>" 重定向符

    ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-12-16-57-06-image.png)
18. tail

    ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-12-17-00-28-image.png)
19. su 切换管理员

    ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-15-21-07-07-image.png)
20. sudo 临时管理员执行

    ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-15-21-10-27-image.png)

    ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-15-21-11-20-image.png)
21. sudo su - root
22. 

## vi编辑器

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-13-19-53-22-image.png)

模式选择与转换

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-13-19-54-03-image.png)

语法基础

生成与编辑文件

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-13-19-56-12-image.png)

基本操作

### 命令模式

   

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-13-20-29-16-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-13-20-32-13-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-13-20-39-45-image.png)

### 底线模式

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-13-20-43-07-image.png)

## 权限

### 用户与用户组

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-15-21-15-11-image.png)

### 创建与删除

#### 用户组创建

1 升为root

2 groupadd name

3 groupdel name

#### 用户组内用户的增删改查

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-15-21-17-53-image.png)

1. useradd
2. userdel
3. id
4. usermod
5. getent passwd 查看用户

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-15-21-26-54-image.png)
6. getent group 查看用户组

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-15-21-26-25-image.png)

### 权限细节

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-17-21-23-19-image.png)

权限细节

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-17-21-24-15-image.png)

其中x：exe执行权限

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-17-21-27-12-image.png)

### 权限修改

#### chmod 权限修改

这里注意，-对象间用，不要加空格

为了确保谨慎，所以这里的R附加项必须大写

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-18-15-32-27-image.png)

简化写法

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-18-15-52-03-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-18-15-52-14-image.png)

#### chown ：chose the owner

目的：修改文件与文件夹所属用户与用户组

特点：只有root能用（分配权限）

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-19-14-29-00-image.png)

## 快捷键

### 强制停止

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-19-14-36-28-image.png)

### 退出或者登出

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-19-14-38-22-image.png)

### 历史指令

history

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-19-14-41-56-image.png)

类似的指令补充，

后缀 tab

前缀 ！

查找CTRL r

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-19-14-46-40-image.png)

### 快速移动

CTRL a  开头

CTRL e 结尾

CTRL → 一个单元

清空

clear

ctrl l

## yum指令

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-19-14-52-37-image.png)

自动化安装。

### 大问题，关于系统的安装，以及对于问题的分析能力的思考

动因：由于虚拟机无法用yum安装wget，所以到处查

1. 思考网络的原因

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-21-21-23-28-image.png)

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-21-21-23-45-image.png)

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-21-21-24-01-image.png)
2. 换源头

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-21-21-24-27-image.png)

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-21-21-24-39-image.png)

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-21-21-24-50-image.png)

[对于Centos 7 不能使用yum源问题解决方法（已解决）_使用centos更新yum源显示权限不足-CSDN博客](http://t.csdnimg.cn/bnouF)

#### CentOS安装wget

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-21-21-27-15-image.png)

#### Ubuntu系统安装

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-21-21-31-10-image.png)

问题：为啥方式不同

安装包和相关程序不同：![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-21-21-31-57-image.png)

#### 后续其他安装

ntp  httpd

## systemctl命令 系统控制指令（软件资源）

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-21-21-36-37-image.png)

对象：系统软件与第三方软件，前提是要注册为系统服务了

操作：开关查设

指令 ，类 函数，对象

特别：

firewalld，注意d

其他：

ntp服务 ->查询对象 ntpd

总结：

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-21-21-47-13-image.png)

## 软连接

简答来说就是创建版的桌面快捷方式

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-22-19-39-56-image.png)

对象：

被链接对象，目的地

效果：

1. 将较长的目标路径，生成一个较短的路径对象（也是文件对象），到短的地方
2. 用ls检查时，形成一个箭头对象来标志(也就表示了生成的对象不具有本体)

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-22-20-05-36-image.png)文件表示为l，line
3. 用 cat 链接 时，可以链接到本体
4. 类似的，用cd 也一样。换句话说，把链接当本题用就行，只是地址指向不同

意义：方便统一管理，分配

## 日期与时区

data

作用：查看时间，自定义显示格式||加减日期，查看对应时钟

用法

1. date
2. date ‘+%__-% __’  其中下划线表示参数，如果显示格式要求空格，需要外部加上引号
3. date -d "+2day"  ‘+%__-% __’

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-23-16-53-02-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-23-16-54-12-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-23-17-02-40-image.png)

时区修改

简单修改

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-23-17-09-19-image.png)

ntp同步

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-23-17-09-51-image.png)

## IP与网络

IP查询

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-23-17-23-59-image.png)

特殊ip

127.0.0.1

0.0.0.0

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-23-17-27-16-image.png)

### 主机名：

代替ip地址，用于内部或者局域网等小范围内表示本机的名称

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-23-17-28-58-image.png)

修改主机名

hostnamectl set-hostname [主机名称]

### 域名解析

本质是将字符地址映射为ip地址的过程。

对象：

1. 本地映射文件
2. DNS服务器
3. 目标网站

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-23-17-35-23-image.png)

流程如下

用户请求域名网站->查询本地映射表->进一步查询DNS服务端->找打IP，进入网站/找不到，寄了

思考：

域名劫持：本地劫持， DNS劫持，服务端劫持或伪装共享。

映射修改：

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-23-17-40-18-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-23-17-40-26-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-23-17-40-35-image.png)

### IP固定

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-24-20-38-27-image.png)

分两步

外层软件指定， 内层Linux配合

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-24-20-39-16-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-24-20-47-00-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-24-20-48-02-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-24-20-48-21-image.png)

Linux的文件配置

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-24-20-46-35-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-24-20-49-33-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-24-20-50-08-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-08-24-20-50-23-image.png)

### 基本网络测试指令

1. ping

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-04-22-35-00-image.png)
2. wget  下载文件

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-04-22-50-29-image.png)

   其中， 用了-b后下载会进入后台下载，

   其显示了日志文件位置与信息

   通过tail指令跟踪下载进度
3. curl 信息请求或者获取文件（前面的c应该是catch）

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-04-22-52-23-image.png)

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-04-22-53-18-image.png)

### 端口的网络意义

区别IP的意义，IP可以锁定网络中的电脑，但是端口可以锁定电脑中的程序

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-05-15-14-07-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-05-15-14-44-image.png)

公共-微绑-临时

#### 端口查看

1. nmap  查看开发端口

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-05-15-20-16-image.png)
2. netstat 查看端口资源占用情况

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-05-15-24-11-image.png)

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-05-15-30-30-image.png)

### 进程

进程管理器与PID

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-11-20-01-17-image.png)

**ps:**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-11-20-02-25-image.png)

注意，这里的STIME指的是累计占用时间

**kill**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-11-20-12-53-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-11-20-14-51-image.png)

### 资源管理器

**top**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-11-20-21-08-image.png)

进程信息

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-11-20-23-44-image.png)

top 扩展指令

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-11-20-25-29-image.png)

top交互式指令

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-11-20-28-02-image.png)

#### 磁盘信息查看

**df**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-11-20-30-08-image.png)

#### CPU资源

**iostat**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-11-20-30-54-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-11-20-40-19-image.png)

#### 网络资源监管

**sar**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-11-20-42-12-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-11-20-43-41-image.png)

### 环境变量

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-12-21-01-23-image.png)

环境变量

1. 是用于记录系统必要指令的准备
2. 本质是一连串的映射表

   反过来，设计一个系统的环境变量，本质上就是基于系统本身的结构，设计一个能将结构内容与指令名称一一对应的一个指令列表

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-12-21-02-08-image.png)

**env**

通过再PATH定义的一系列文件地址，去搜索指令的文件。

反过来，如果需要添加一个指令，也只需要再path对应位置给出相应的执行程序即可

**echo + \$ 环境变量名称**

返回后面的环境变量的对应key值

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-12-21-08-28-image.png)

#### 环境变量的设置

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-12-21-13-27-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-12-21-15-19-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-12-21-25-47-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-12-21-26-36-image.png)

## 上传与下载

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-12-21-33-58-image.png)

## 压缩和解压缩

压缩格式

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-13-18-02-50-image.png)

1. tar 简易封装压缩

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-13-18-04-00-image.png)

   常用组合

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-13-18-06-34-image.png)
2. 解压gzip

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-13-18-19-07-image.png)

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-13-18-22-45-image.png)
3. 压缩zip

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-13-18-23-17-image.png)
4. 解压unzip

   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-13-18-25-50-image.png)

   **如果同名，默认覆盖**

## 基本Linux专业软件

### Mysql管理

1. 5.7版本

   1. centos

      ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-09-13-18-37-23-image.png)

## Linux 即Ubuntu下的编译

### vim配置

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-16-12-25-image.png)![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-16-11-20-image.png)

### vim编写与编译

编写基本上是类同的，但是main函数可能需要补全其请求参数；

gcc编译 一般自带，编译平台为X86。如果在arm上编译需要用交叉（平台）编译器

主要用

gcc file -o newName

### Make软件与makefile文件

**多文件**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-17-07-51-image.png)

问题：

1. 多文件编译输入麻烦
2. 直接生成可执行文件的话，需要每次编译时，同时编译所有我文件

方法：

对于每个.c文件进行单独的gcc -c 编译为.o文件

然后再对main.o文件进行gcc -o 链接生成文件

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-17-14-22-image.png)

#### MakeFile如何写

**手写**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-17-24-31-image.png)

生成对象：依赖对象 依赖对象

    生成对象生成方式

子生成对象：依赖对象

    生成对象的生成方式

**Makefile应用**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-17-37-11-image.png)

Makefile中的变量：便于简化函数名文件等

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-18-36-52-image.png)

parm = name1 name2 name3

\$(parm)

#### 模式化：对于重复文件的解决

##### 可变变量与指代量

**\$()本质是取别名，并不是复制其值**

直接=

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-21-15-52-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-21-15-38-image.png)

因此，也就有了对应的强制取值:=

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-21-20-01-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-21-20-15-image.png)

默认赋值:如果前面左边的对象没有被赋值，那么就赋值，反之就不起作用。

?=

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-21-25-37-image.png)

补充追加：

+=

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-21-25-24-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-21-25-35-image.png)

**指代**

问题：在用%通配指代对象的时候， 由于gcc指令需要具体指令对象，因此不能也用%进行通用匹配。

如下就出现了：自动化变量：

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-21-37-20-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-21-43-29-image.png)

#### 伪目标：对于重名问题的解决

.PHONY: target

target:

    work1

伪目标，其作用是在调用make target时，无论是否存在target

target下的work都会执行

## Shell脚本:

**基本思想原则：所有的命令，都是独立的语句**

问题：每次问题都要反复编程解决。

指令集合程序

特征：

name.sh：内容

\#!/bin/bash

实现：

1. 在文件下编写本来就可执行的代码
2. 查询文件权限，通过chmod 777 文件赋予权限
3. ./运行文件

### 语法：

基本模型

变量如果可能有歧义， 往往\$

#### 读入：

**read**

基本交互：

输入输出

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-22-13-02-image.png)

多变量读取

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-22-19-49-image.png)

交互与运算

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-19-22-27-35-image.png)

**注意，等号附近不要有空格。也就是要留意target是否被识别为变量**

---

#### 判断：

**test测试 &&继承于||互斥**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-20-16-24-07-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-20-16-37-15-image.png)

注意点在于，把每一个小语句都认为时一种指令。

**中括号[ ]**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-20-16-43-44-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-20-16-47-45-image.png)

如果不加“”使得接受为整体对象，那么用于判断的时候就会分别当成独立的参数了

---

#### 变量扩展：

在用某个shell脚本的时候，为了让它如同指令一样的实现， 指令 参数 参数的格式。我们引入了默认变量：其将脚本与其输入的变量进行了标号实现通配调用

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-13-31-31-image.png)

```shortcode
$0  指代的是本文件
$#  表示接受的参数量
$@ 表示接受的参数列表
$1   表示列表第一个变量
```

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-13-31-16-image.png)

### Shell的if-else for

对于要执行的分支语句比较多的的时候用。

形式上，其实与Matlab有点像

**if then**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-13-44-13-image.png)

注意，**`<mark>`then前面的是分号， 结束居然是fi`</mark>`**

注意， 空格也很麻烦

**if else**![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-13-50-19-image.png)

**elif**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-13-53-19-image.png)

### case语句：逆天

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-14-05-24-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-14-03-54-image.png)

注意：**星号通配的话，不要引号**

### function函数

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-14-12-12-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-14-11-30-image.png)

**代码内部传递参数**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-14-17-23-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-14-17-06-image.png)

**while循环**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-14-22-17-image.png)

注意：`<mark>`【】中的四个地方（等号两边，【】靠近)都是要有空格的。`</mark>`

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-14-21-53-image.png)

**for循环**

整体上：类似于python的遍历形式

变量遍历：

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-14-26-14-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-14-26-02-image.png)

值遍历：

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-14-30-17-image.png)

注意：`<mark>`这里的do里面的total前面没有\$ 应该是用后面的部分来表示了`</mark>`

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-21-14-30-04-image.png)

## 裸机开发

### 文件互传

装vsftpd（client即可）

装nsf

装ssh

装ch340驱动

装交叉编译器 linaro

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-24-21-38-41-image.png)


wocheyixia

    复制并解压到/usr/local/arm后，添加环境变量

> 题外话：环境太多限制，导致开发的板子必须要小心翼翼。

安装vscode到虚拟机

## 书籍推荐集合

shell语法：鸟哥的Linux私房菜
