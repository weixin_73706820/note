# 逻辑开发

## Cortex-A汇编

1. 需要通过汇编初始化SCO部分外设。比如停止某个寄存器的效用（复位等）

2. 初始化DDR ， imx6可以不用。（内部有写好的引导

3. 设置sp指针，指向DDR， 设置好C语言运行环境（现场保护等）

启动引导与环境准备。

### LED灯的实现流程

#### LED灯的基本配置原理

STM32的初始化工作

1. 使能GPIO时钟时钟

2. <mark>设置IO复用，使其复用为GPIO</mark>

3. 配置GPIO结构体，

4. 使能设置，输出IO信号。

IMX6UL

1. 原理图分析， 找到对应外设对应的GPIO口

2. 找到手册，对应的GPIO口的部分

3. 找到外设驱动模块手册CCGR

4. CCGR0-CCGR6控制全部外设（全程为CCM_CCGR)
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-26-14-17-47-image.png)
   
   区别于STM直接的enable，IMX有着更多种类的使能方式。

5. 设置CCGR
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-26-14-19-07-image.png)
   
   区别于STM，IMX6U的寄存器为三十二位，其中每个两个位对应一个特殊外设的时钟。状态如上方相同。
   
   0xFFFF FFFF 全部打开

6. 打开时钟后，开始开启GPIO设定GPIO的用途（也就是关联到内部的哪个部件），这里是IO
   
   起初是这个IOMUXC_SW_<mark>MUX</mark>_CTL_PAD_GPIO1_IO03
   
   这里的SW，即stop- wait协议。
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-26-14-40-24-image.png)
   
   将这个寄存器bit0-3位设置为0101, 使得GPIO1_IO03复用位GPIO

7. 配置GPIO的各种参数（也就是STM的结构体参数）
   
   IOMUXC_SW_PAD_CTL_<mark>PAD</mark>_GPIO1_IO03（这里的pad就表示了他是物理的焊盘参数）
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-26-14-36-10-image.png)
   
   1. 配置这个参数，重要的是了解两个东西
      
      一个是他的参数对应的电路图像
      
      另一个是参数的设置范围
   
   2. 主要配置参数
      
      压摆率， 开漏输出，速度， 驱动，上拉。

8. 明确GPIO功能（特殊步骤， 也就是设置输入还是输出）
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-26-15-08-23-image.png)
   
   > 到这里其实我们可以有一个思考，那就是芯片的配置其实也是类似STM32一层层下辖的。
   > 
   > 从GPIO于GPIOCCM--GPIOMUX和GPIOPAD--GPIOPAD部分又由各自的下辖
   > 
   > 我们的声明是先打开时钟。然后从下到上，一步步声明的。
   
   DR寄存器（数据寄存器，输出时存要发的， 输入存接受的）。
   
   GDIR寄存器， 控制GPIOx_pin方向的。
   
   其中ICR寄存器位中断管理寄存器（由于每个ICR控制16个Pin的中断，但是一个GPIOx组有32个引脚，因此需要两个）
   
   IMR，中断发生判定器。

9. 配置上级的GPIO管理寄存器
   
   GPIR，设置方向位向外， bit3 = 1设置位输出模式
   
   DR，设置高电平，bit3 = 1 设置输出的值位1

#### 汇编代码实现（其实是我们STM的相关core前置函数)

作用时为C语言准备环境（例如出栈入栈），这个就需要SP指针，指向栈顶保存函数。因此只有初始化好SP指针，才能有栈，指向对应内存。

部分特殊芯片，需要另外设置内存的相关问题（DDR2等）

**重点的，汇编格式在不同芯片上是不同的**

这个遵从GUN语法。

因此，会涉及到各种汇编指令部分（需要描述的更加具体）

##### 读写内存 LDR与STR

若a地址0x30， b0x40

LDR R0， =0x30

LDR R1， [R0]    @寄存器 《-内存

LDR R1， =0x40

STR R1， [R0]  @内存-》内存

##### 跳转

#### 处理器模式

ARM有7+2种模式在coreA 7种。（STM有两种运行模式）

内部有R0-R15共计16个通用寄存器。不同寄存器不同功能。不同模式下，分配到寄存器功能也不同（显然就是不同模式，需要的功能强度不同）

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-26-16-14-32-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-26-16-14-03-image.png)

#### 具体编写

1. 声明全局标号。

2. 查找时钟，配置时钟寄存器，这就需要寄存器地址
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-16-02-03-image.png)
   
   显然，我们得知是CCGR控制对应的GPIO外设。
   
   然后找到对应的CCGR看下情况，发现CCGR0描述如下
   
   1![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-16-14-51-image.png)
   
   说明了寄存器内的对应控制外设用的子寄存器位数为两位，所以控制外设的时钟的寄存器可以有四种状态。
   
   2![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-16-16-01-image.png)
   
   CCGR0有32位，对应16个外设的寄存器。

3. 基于CCGR0的设置不同外设时钟的情况，我们尝试全部时钟都完全打开，即0xFFFF FFFF
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-16-21-45-image.png)

4. 继续寻找其他时钟控制寄存器。
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-16-22-50-image.png)
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-17-23-19-image.png)

5. 时钟控制寄存器设置完成后，设置IOMUX寄存器模块下的，GPIO复用模块
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-17-23-58-image.png)
   
   发现，设置为0101模式即为复用GPIO模式，回实例化GPIO1模块下的GPIO1_IO03
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-17-24-48-image.png)
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-17-40-18-image.png)

6. 配置完复用模块后，配置电气属性：
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-17-40-04-image.png)
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-18-24-53-image.png)

7. 配置内容控制与方向
   
   先要找到基地址：
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-18-25-33-image.png)
   
   具体到GDIR的内容时，即为每个IO口一个输入输出模式
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-18-29-41-image.png)

8. 配置（这里有个地方比较特别，那就是外部时高电平的（看原理图可知），内部拉低就开灯了
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-18-39-22-image.png)

9. 最后，回环控制
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-18-39-37-image.png)

#### 编译验证

<mark>编译流程本身如下</mark>

源文件（.c,.s）--（编译）-》.o文件 ---(链接) ---->.elf可执行文件---->bin文件（用于烧录)|

                                                                                            | -------------->反汇编，                  |

                                                                                                                                                    |

                                                                                                                               用于烧录的.imx

**<mark>生成.o文件</mark>**

首先，这就需要交叉编译器了（已经装入的情况）

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-15-18-51-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-27-18-42-20-image.png)

**<mark>生成elf</mark>**

链接有两个作用，

1. 将所有编译产生的.o文件都链接到一起

2. 将链接的文件同时也链接（映射到）一个指定的内存位置（便于执行的时候查找调用？）。

<mark>特别的：链接起始地址就是代码执行的起始地址</mark>

stm32的例子，在STM中，起始地址是flash内部。![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-15-26-17-image.png)

默认链接器

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-15-27-08-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-15-20-27-image.png)

但是在cortex 系列中，flash等是内部的ROM，不可被改写。因此

<mark>我们的程序链接后的起始地址应该是RAM（内存）内的</mark>

然后，RAM又分为内部（RAM）和外部（DDR）（尤其是DDR这种高效的存储器）----这里的一个疑问是，stm32的flash作为非易失性的存储介质，用来存这些引导地址是合理的。但是RAM作为易失性的存储介质，为什么可以作为引导。----》实质上是，每次都会从数据来源（SD）卡上加载。（这也是为什么一定要烧录后才能运行的原因把）

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-15-49-17-image.png)

<mark>内部RAM的地址范围是</mark>

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-15-35-14-image.png)

<mark>而DDR的范围则是</mark>

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-15-45-48-image.png)

下面的图512的范围有点问题，参考上面的。

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-15-47-44-image.png)

到此，其实我们就大概清晰了整个裸机代码的运行路线了。

1. 上电

2. 内核初始化，调用内部引导boot RAM 读取SD卡中的可执行文件ELF等拷贝到链接区

3. 这个链接区有两个
   
   1. 内部RAM
   
   2. 外部DDR

4. 内存内容准备完毕后，处理机开始处理。

<mark>关于DDR与Bin文件</mark>

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-15-54-57-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-15-57-53-image.png)

概括来说是这样的：

在编写时，对于DDR的初始化不需要编写者操心。因为在DDR外设的内部存在自身的一些基本初始化文件。而SOC内部的boot rom（如果是匹配的对应外设的话）会有相应的引导信息，自动读取外设，初始化对方。

然后，准备好了外设。就将SD内的bin放到对应地址处。

而在我们编写程序的时候只需要考虑一件事情：

<mark>编写程序的链接的地址，与你程序中要用的地址（除非另外开辟了逻辑地址，并且做好了变</mark>换？）应该要一致

例如：如果你链接的位置为0x8780 0000的话，那么程序中使用的地址就必须是0x8780 0000+x(不超过范围)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-16-03-36-image.png)

##### 编译时指定链接地址

显然，根据我们刚才的了解。在生成ELF时，我们要指定编译生成的文件的具体链接地址（不然cortex 读取SD的时候，读取ELF头文件，发现没指定地址，他也就不知道把文件放哪了）

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-16-08-02-image.png)

**<mark>生成bin</mark>**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-16-11-29-image.png)

**对应选项不能小写哦**

> elf 一般用于系统
> 
> bin 一般用于底层直接的裸机（无系统）的设备。例如单片机等

**<mark>反汇编</mark>**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-16-15-26-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-16-16-08-image.png) 

#### 烧录

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-16-19-15-image.png)

测试：

如果后面没反馈，证明

exFat可用

如果反馈

则需要分卷， 分16g，然后用软件格式化为fat32

烧写不同于拷贝。

烧写需要指定你写入SD（tf）对象的绝对位置

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-16-33-11-image.png)

需要专业软件了。

其中，不同的芯片需要的绝对位置不同，因此需要不同的软件。例如IMX家族需要特定的IMX下载器。

1. 查找sd卡情况

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-16-39-05-image.png)

估计，dev应该时外设的意思了。

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-16-39-59-image.png)

发现只变换了一个有对应驱动的sdb有sdb1

找到需要烧入的对象后，给软件赋予权限。

然后就是执行即可。

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-16-46-50-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-16-47-24-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-16-37-41-image.png)

这里有个细节

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-16-47-47-image.png)

我们烧入的其实不是bin，而是load.imx文件。其实也可以理解，就是要转化为厂商自己的文件系统嘛。（过程其实说是加了个头部）

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-16-51-04-image.png)

烧入完成后，终于可以将SD卡插入板子中，然后看他上电后，等两秒，开始发光。

#### 基本点亮物理流程

上电

|

系统内核（内部ROM相关程序）启动。同时，初始化一些基本外设，如DDR

|

读取boot引脚状态，进入对应引导模式

|

检测到为SD卡模式（1000 0010的boot）

读取SD卡

|

SD卡内部被烧录的imx程序被读取。配置相关外设。

|

被读取的程序由bootROM 被放入到指定的RAM区域

|

读取完毕，开始执行RAM内的程序。

|

LED点亮。

#### 如何简化编译（Makefile的编写）

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-17-24-47-image.png)

为什么不能如同STM32那样直接Link烧录进去

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-28-17-38-02-image.png)

### IMX的启动

两个部分：

硬件

启动文件

之前我们通过SD卡启动。因此6ULL可以SD卡启动。

类似的各种启动，例如flash（NAND， EMMC）， Norflash， spiflash， SDnand等的等。

#### 手册地点

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-13-51-38-image.png)

#### 硬件的引导配置

在这里，我们的思路时这样的。

思考我们需要实现什么样的功能：SD引导。

1. 芯片中配置引导方式的内部寄存器----BOOT_MODE

2. 直到配置了引导后，如何启动引导需要的设备-的内部寄存器---BOOT_CFG设备

3. 思考如何配置这些内部寄存器----对应的外设引脚图---引导引脚。

##### BOOT_MODE启动模式

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-13-53-30-image.png)

（fuses熔丝， 这两个引脚就是下面的引脚的前两位）

这两个boot引脚，通过IO控制。

核心版图：

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-13-54-49-image.png)

底板图：

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-13-59-59-image.png)

（这里的OFF，与ON反了，默认OFF下拉）

显然，是开漏输出（在拨码开关处看出），那就需要下拉电阻在接受端。显然在核心板处没发现的话，应该是有内部下拉配置（也是常规搭配了）

##### 直接烧写如何实现

选择USB下载等，下载相关程序到SD或EMMC等， 然后启动**内部BOOT启动** ，这样就能实现基于内部存储介质的启动了。

##### 启动设备的选择

当选择内部设备启动时（包括SD卡， 至少不包括usb等外部的而已。|即mode1=1， mode0=0）

需要通过后续的设备选择键位选择对应的boot引脚。

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-14-15-05-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-14-20-31-image.png)

不同需求，启动不同的设备引导。

##### 配置引导启动引脚链（MODE_CFG的GPIO映射）

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-14-27-02-image.png)

通过对应的表格，我们直到，有三个BOOT_CFG才能配置完引导模式，以及对应的外设。

思考，怎么配置这些内部的东西呢？很显然，是GPIO外设吧。那就要找映射表了。

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-14-37-24-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-14-37-49-image.png)

对应的芯片封装如下：

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-14-38-21-image.png)

而我们的板子的引出情况如下：

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-14-38-45-image.png)

其中，除了标记DIP的引脚，全部都是默认接地。

且cfg4的全接地。因为没用过， 其每个寄存器的功能图对应底板左边的封装图。

#### 关于LED驱动启动的细节流程

结合上面我们学习的BOOT引脚配置引导的事情。我们可以更加深入的理解这个驱动过程

**硬件上**

1. BOOT_MODE（不是BOOT_CFG,那是设备配置）设置为10,设置为从内部开始启动
   
   ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-15-17-44-image.png)

2. 然后根据CFG执行加了头的BIN文件（便于定位一些地址啥的），初始化设备，运行。

问题是，第二个过程中到底发生了什么？我们如何实现第二个内容。

答案是：通过cfg的配置，读取外设中的镜像img时， 做相应的处理。流程大概如下

初始化时钟信号--可能启用MMU和Cache加快--->验证完image

**软件上的处理**

在之前的LED编写过程中。我们思考到，对于STM32我们可以通过STLink烧录到内部的flash来直接执行hex/bin文件。（顺便解释，bin文件就是纯粹的执行文件，即处理机所接触到的全部文件， 而hex文件添加了一些头部，比如写入渎职的指定和校验,不然bin文件每次写还要先挪动好地址指针才行)

但是对于IMX6UL来说，则必须还要编译为.imx(其实根据上面也大概猜到原因了)， 两者的区别时：

.imx文件包括：

1. image vector table 即为简称 IVT， IVT 里面包含了一系列的地址信息，这些地址信息在  
   ROM 中按照固定的地址存放着。

2. Boot data，启动数据，包含了镜像要拷贝到哪个地址，拷贝的大小是多少等等。

3. Device configuration data，简称 DCD，设备配置信息，重点是 DDR3 的初始化配置

4. bin文件

显然，我们主要是多了：地址，配置两个部分。

反过来，如果我们在写入bin前初始化好这些部分，就可以直接写入bin了。

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-15-50-00-image.png)

##### IVT与BootData

**IVT**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-15-51-53-image.png)

简单来说，就是告诉引导读取者，应该把我烧录到哪个存储器的哪个位置的指针。

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-15-59-44-image.png)

内容上：

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-16-00-55-image.png)![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-16-01-39-image.png)

**BootData**

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-16-02-49-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-30-16-06-40-image.png)

IVT的具体结构：

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-31-21-08-32-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-31-21-09-23-image.png)

基于这个，其实我们也可以自己编写属于自己的imx生成器？

基本上，就是地址，还有需要接受bin并计算大小，转化为偏移地址。



##### DCD数据

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-31-21-13-34-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-31-21-13-55-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-31-21-14-47-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-31-21-18-43-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-31-21-19-01-image.png)



小结：

目的上,我们的逻辑时这样的：

思考我们的bin的纯二进制文件，是如何被imx6ull理解的。

即，为什么要转化为.imx文件

即imx文件于bin的关系

引出了 vix于DCD---分别的作用是起头导入，于寄存器配置。



vix用于导入开始，指定数据开头地址与数据整体大小。

DCD主要是各种需要配置的数据。



其中，之所有能够被imx理解，是因为他们准从发送的结构与格式（大端）。



## Cortex的C语言环境与准备

上面的汇编过程，是为了给C语言环境做准备的。



而设置C语言环境，有几个步骤

1. 设置处理器模式为SVC模式：也就是配置CPSR寄存器（参考在下面）|stm默认特权模式
   
   1. 读写需要特殊指令（也就是LDR， STR不行了）
      
      ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-31-21-40-04-image.png)

2. 设置sp指针：用于C语言执行
   
   SP指针可以执行内部RAM与DDR。 我们指向DDR。
   
   具体设置：
   
   1. 基于DDR大小， 我们直到起始地址是0x8000 0000~ 9FFF FFFF（512M).
   
   2. 确定栈的大小， 0x20 0000h=2MB
      
      ![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-31-21-48-19-image.png)
   
   3. 如何实现栈的功能
      
      1. 如果向上增长， 那就指向0x8000 0000+size = 0x8020 0000;

3. 跳转到C函数：main等。解析有包估计
   
   1. 编写代码





CPSR：

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-31-21-35-47-image.png)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-31-21-35-26-image.png)



### 实现代码



#### 基本流程

start文件（负责启动imx内核）--> 



#### start文件编写



start.**S**文件(这个是用于放入内存的时候用的， 编译开启.imx文件的引导头， 同时设置imx基本模式|同时，设定C语言环境，准备指针)

![](C:\Users\zhang\AppData\Roaming\marktext\images\2024-10-31-22-11-25-image.png)

设置完成模式和指针后，直接开始main函数调用

![image-20241102162514327](assets/image-20241102162514327.png)



####  寄存器环境

![image-20241102152217208](assets/image-20241102152217208.png)

#### 主体函数

![image-20241102153604399](assets/image-20241102153604399.png)



#### 对应的makefile文件

![image-20241102162630853](assets/image-20241102162630853.png)

注意

![image-20241102155906828](assets/image-20241102155906828.png)



![image-20241102162616424](assets/image-20241102162616424.png)

#### 对应的操作

![image-20241102170138491](assets/image-20241102170138491.png)

#### 闪烁操作



![image-20241102171001024](assets/image-20241102171001024.png)

**闪烁还是很开心的**



#### 链接脚本lds

![image-20241102172004584](assets/image-20241102172004584.png)

简单来说，链接脚本的作用，是在通过源代码，生成.elf等文件时，**用于描绘文件段结构（这里和操作系统课上的知识就关联了），便于存放不同类型的数据到不同的地方**。也确实，在操作系统课程中，我们学习的段页式内存管理那一章也有提到，页本身是由操作系统来分配的，但是段（按照功能的含义分配的部分）则是由程序员实现的。

![image-20241102172401883](assets/image-20241102172401883.png)



结构解释如下

`SECTIONS{`

`.= 0X10000000 @这里的.类似指针，指向内存位置，这里赋值`

`.text : {*(.text)} @.段名 :  要连接的文件{通配符(.text所有文件中的text段放入)}`

`.data ALIGN(4) : { *(.data) } @ALIGN(4)段起始地址字节对齐，表示data的起始地址要求被4整除`

`.bss 			       @用于定义没有被初始化的变量`



例如

①、链接起始地址为 0X87800000。
②、 start.o 要被链接到最开始的地方，因为 start.o 里面包含这第一个要执行的命令  

```c
SECTION{
    . = 0x87800000;
    .text:
    {
        start.o
        *(.text)
    }
    .rodata ALIGN(4) : {*(.rodata)}
    .data ALIGN(4) : {*(.data)}
    __bss_start=.;
    .bss ALIGN(4) : {*(.bss) *(COMMON)}
    __bss_end=.;
}
```



>**总的来说，有几个重点**
>
>1. '.' 号是个指针
>2. .名称 就是声明了一个标签，也就是命名当前位置为： 段名
>3.  ALIGN就是帮确保名的位置能取整
>4. :后面就是说，这里开始要放哪一类数据
>5. =.表示将当前的地址给出
>6. **作用，代理原本的makefile文件中的Ttext的部分。同时，如果此时你再次打开优化功能的话，页不会有被优化位置的风险**
>
>
>
>![image-20241102180150276](assets/image-20241102180150276.png)
>
>优化加上
>
>![image-20241102180207413](assets/image-20241102180207413.png)

### 近似STM32的操作方法

![image-20241105212118887](assets/image-20241105212118887.png)

![image-20241105212210003](assets/image-20241105212210003.png)

stm32通过结构体指针来实现的调用的。

指针结构体-》结构体内部外设 = 赋值（初始值指向寄存器基地址）。

而这个RCC_BASE是stm32寄存器的基地址。然后，基于寄存器本身的大小，移动指针即可（注意位置对应）





#### 初始化工作区

之前没有过多关注，但是在多次测试后发现，如果不及时生成工作区。那就不能进行代码间的联合编译了。

![image-20241105213523315](assets/image-20241105213523315.png)





#### 初始化start文件

上个小结，我们直到用imx程序将起始文件进行了分段。

![image-20241105214034875](assets/image-20241105214034875.png)

但是我们的start文件其实本来就是做启动的。为什么不直接合并呢？

![image-20241105214313268](assets/image-20241105214313268.png)

这是原来的start文件，只有启动cpu的SVM模式的功能，然后开指针，没有更进一步的功能了，因此其他的定位和段分配功能需要另外开。

改变后：

​	![image-20241105215109563](assets/image-20241105215109563.png)



因此，修改后的start文件如下

```c
.global _start

.global _bss_start      /*估计，只能用_杠在代码中 */
_bss_start:
    .word __bss_start   /*类似声明一个变量，注意这里是__ */

.global _bss_end
_bss_end:
    .word __bss_end

_start:
    mrs r0, cpsr
    bic r0, r0, #0x1F
    orr r0, r0, #0x13
    msr cpsr, r0

    /*清除BSS段 */
    ldr r0, _bss_start
    ldr r1, _bss_end
    mov r2, #0
    /*ldr用=赋值， mov用# */

bss_loop:
    stmia r0!, {r2}     /*将r2的0->r0(也就是start指针所指), 然后地址自增 */
    cmp r0, r2          /*比较r0与r1的值，直到初始化完bss区 */
    ble bss_loop        /*如果r0小于等于r1， 回条*/
     
    ldr sp, =0x80200000
    b main
```

这里着重理解的就是变量的声明，以及循环实现。



**到此，我们的cpu初始化，段落初始化，定位基本完成**



#### 时钟初始化

![image-20241105221256387](assets/image-20241105221256387.png)

这些是初始化需要的时钟（类似stm32中的RCC）

​	而由于地址的线性移动，需要把全部的结构体都初始化了。

​	基本初始化如下

```
/*
目的：初始化外设寄存器，形成结构体
目标: CCM外设

*/

//偏移+base
#define CCM_BASE 0x020C4000         /*也就是CCM的寄存器的第一个寄存器的地址*/
typedef struct 
{
    volatile unsigned int CCR;      /*这里需要重点注意，（也是因为C结构体地址连续)地址连续，且变动的大小相同。*/
    volatile unsigned int CCDR;     /*即使不够也要添加占位 用一个RESERBED_1[跨的字节数，这里一个寄存器位2]*/
    volatile unsigned int CSR;
    volatile unsigned int CCSR;
    volatile unsigned int CACRR;
	volatile unsigned int CBCDR;
	volatile unsigned int CBCMR;
	volatile unsigned int CSCMR1;
	volatile unsigned int CSCMR2;
	volatile unsigned int CSCDR1;
	volatile unsigned int CS1CDR;
	volatile unsigned int CS2CDR;
	volatile unsigned int CDCDR;
	volatile unsigned int CHSCCDR;
	volatile unsigned int CSCDR2;
	volatile unsigned int CSCDR3;	
	volatile unsigned int RESERVED_1[2];
	volatile unsigned int CDHIPR;  
	volatile unsigned int RESERVED_2[2];
	volatile unsigned int CLPCR;
	volatile unsigned int CISR;
	volatile unsigned int CIMR;
	volatile unsigned int CCOSR;
	volatile unsigned int CGPR;
	volatile unsigned int CCGR0;
	volatile unsigned int CCGR1;
	volatile unsigned int CCGR2;
	volatile unsigned int CCGR3;
	volatile unsigned int CCGR4;
	volatile unsigned int CCGR5;
	volatile unsigned int CCGR6;
	volatile unsigned int RESERVED_3[1];
	volatile unsigned int CMEOR;
}CCM_Type;

#define CCM ((CCM_Type *) CCM_BASE)  /* 结构体指针CCM_Type* 指向CCM寄存器的基地址，因此可以通过CCM访问结构体 */
```

而其他的时钟与寄存器同样也需要设置。这就需要反复核验了。



#### 编写C文件



初始化时钟

![image-20241106214401651](assets/image-20241106214401651.png)

结果代码如下

main.c

```c
#include "imx6u.h"                      //!!!!!!!!

//  使能外设时钟
void clk_enable(void) {
    CCM->CCGR0 = 0xFFFFFFFF;
    CCM->CCGR1 = 0xFFFFFFFF;
    CCM->CCGR2 = 0xFFFFFFFF;
    CCM->CCGR3 = 0xFFFFFFFF;
    CCM->CCGR4 = 0xFFFFFFFF;
    CCM->CCGR5 = 0xFFFFFFFF;
    CCM->CCGR6 = 0xFFFFFFFF;

}


// Initialize the LED
void led_init(void){
    //  IOMUX 的MUX的Cell，以及对应的电器属性
    IOMUX_SW_MUX->GPIO1_IO03 = 0x5 ;  // 选择io3为GPIO1_IO03
    IOMUX_SW_PAD->GPIO1_IO03 = 0x10B0;//设置GPIO1_IO03的电器属性

    // 初始化GPIO数据寄存器与方向寄存器，防止数据和设定方向
    GPIO1->GDIR = 0x8;          //GPIO1_IO03,也就是bit3设置为输出
    GPIO1->DR   =0x0;           //设置为输出数据，打开LED        
}

void delay_short(volatile unsigned int n){
    while(n--){}
}

//延时函数 1ms, 招聘369
void delay(volatile unsigned int n){
    while(n--){
        delay_short(0x7ff);  // 1ms 延时
    }
}

//打开led
void led_on(void){
    GPIO1->DR &= ~(1<<3);  // 将1 左移3 1000 取反 0111 去与 因此只有0处归零了
}
//关闭led
void led_off(void){
    GPIO1->DR |= (1<<3);  // 将1 左移3 1000 取反 0111 去与 因此只有1处置1了
}
int main(void){
    //setup the LED ligthing
    clk_enable();
    led_init(); 
    while(1){
        led_on();
        delay(500);
        led_off();
        delay(500);  // 2s 闪烁
    }

    return 0;
}
```

特点就是寄存器的问题。



#### 编写Makefile同编译文件

```c
objs 	:= start.o main.o		#变量定义
ld 		:= arm-linux-gnueabihf-ld
gcc		:= arm-linux-gnueabihf-gcc
objcopy := arm-linux-gnueabihf-objcopy
objdump := arm-linux-gnueabihf-objdump

ledc.bin : $(objs)				#基于这个，展开
	$(ld) -Timx6u.lds -o ledc.elf $^  #变量带入，插入链接文件， 生成elf, 基于依赖文件
	$(objcopy) -O binary -S ledc.elf   $@ 	#将elf文件转化为没符号信息的elf文件
	$(objdump) -D -m arm ledc.elf > ledc.dis	#反汇编 -D生成反汇编， -m 指定架构

%.o : %.c
	$(gcc) -Wall -nostdlib -c -O2 -o $@ $<

%.o : %.s
	$(gcc) -Wall -nostdlib -c -O2 -o $@ $<

clean:
	rm -rf *.o ledc.bin ledc.elf ledc.dis
```



段分配文件imx6u.lds

![image-20241107201007633](assets/image-20241107201007633.png)

其中，对应的段在start.S中即可找到

![image-20241107201147681](assets/image-20241107201147681.png)





奇怪的是，编译后的dis文件中， start的初始位置不再87800000导致打不开灯，很是奇怪



### 移植SDK

准备SDK文件---》准备编译环境（头文件，包括数据格式与类型声明）--》明确需要移植的文件，传入-->修改移植文件，保留需求部分 ----》 生成验证文件，makefile，编译尝试验证



先要将对应的移植exe下好，然后生成

![image-20241107211556253](assets/image-20241107211556253.png)

![image-20241107211542341](assets/image-20241107211542341.png)

其次是明确我们的目的，即，

![image-20241110195924524](assets/image-20241110195924524.png)



新建cc.h文件，用于配置sdk相关前置环境/主要包括数据的命名或结构的定义，指定一些内容。

![image-20241110201102473](assets/image-20241110201102473.png)



传入文件：

![image-20241110201345965](assets/image-20241110201345965.png)

估计本别是通用配置文件，外设io中的mux胞的配置文件， 以及主硬件初始化库。



在找移植文件时，可能会遇到同名的文件，需要到上级目录查找下对应的类型与要求

![image-20241110201523892](assets/image-20241110201523892.png)

我们的MCIMX6Y2， 所以移植要用这个文件夹的移植文件



接着，就需要根据视频/或者内部需求，修改相关的头文件（我估计实际开发中一般是直接整个sdk搬运的，所以没必要修改声明，除非报错）



主要的变化就是用函数的地方（其实和到这里，和STM32的标准库用法就差不多了）

![image-20241110205315544](assets/image-20241110205315544.png)

![image-20241116172405694](assets/image-20241116172405694.png)

![image-20241116172621368](assets/image-20241116172621368.png)

![image-20241116172747411](assets/image-20241116172747411.png)

上图对应于

![image-20241116172921664](assets/image-20241116172921664.png)

在宏中的解释。

![image-20241116173427122](assets/image-20241116173427122.png)

例如如下外设

![image-20241116173455907](assets/image-20241116173455907.png)

路径

得到要配置外设的寄存器地址--->基于地址，配置寄存器的复用电器属性，配置使能



换句话来说，这种配置就是利用了配置外设时，需要寄存器位置， 寄存器值。的特点来进行了分离。将寄存器值中可以分类的部分单独列出来几个参数，这样可以在宏定义的时候利用参数的不同，写出不同的宏名。



编写Makefile

![image-20241110215138824](assets/image-20241110215138824.png)



### BSP工程管理

也就是基于文件属性进行文件的模块化管理。

![image-20241116175042277](assets/image-20241116175042277.png)

上节课代码修改后的结构

![image-20241116180740712](assets/image-20241116180740712.png)

驱动模块分配到bsp

![image-20241116193147941](assets/image-20241116193147941.png)

结果如图。特别的地方在于，main.h也可以有。其他部分和STM32的模块分离差不多，都是头文件和函数文件分离。



#### 设置编译器（vscode）头文件路径

![image-20241116193707823](assets/image-20241116193707823.png)

![image-20241116193850604](assets/image-20241116193850604.png)

![image-20241116194535406](assets/image-20241116194535406.png)

如果编译的时候找不到头文件，可以在工作区添加路径（因为可能路径比较深），注意后面的逗号



#### MakeFile的重新编写

```makefile
CROSS_COMPILE	?= arm-linux-gnueabihf-
TARGET			?= ledc
#要生成的文件对象

CC				:=${CROSS_COMPILE}gcc
LD				:=${CROSS_COMPILE}ld
OBJCOPY			:=${CROSS_COMPILE}objcopy
OBJDUMP			:=${CROSS_COMPILE}objdump

#头文件目录路径 续行
INCUDIRS		:= imx6u \
				   bsp/clk \
				   bsp/led \
				   bsp/delay

SRCDIRS			:= project \
				   bsp/clk \
				   bsp/led \
				   bsp/delay


#调用函数, patsubst(要替换的对象，替换的对象(-I %任意长度), 替换对象)
#INCLUDE = -I imx6u -I bsp/clk -I bsp/led -I bsp/delay
INCLUDE			:= $(patsubst %, -I %, $(INCUDIRS))

#保存汇编文件
#wildcard通配符函,用于查找路径下的某类文件。
#foreach (临时变量, 目标组合量， 操作) 每次江目标组合量取出--》放到dir临时-->执行操作
SFILES			:= $(foreach dir, $(SRCDIRS), $(wildcard $(dir)/*.S))
CFILES			:= $(foreach dir, $(SRCDIRS), $(wildcard $(dir)/*.c))

#简单来说
#dir 用于生成指令的中介量
#include 指令路径，可能会结合指令指定，如-I
#XFILES 存储某类文件路径的变量


#notdir 去除路径，保留文件名
SFILENDIR		:= $(notdir $(SFILES))
CFILENDIR		:= $(notdir $(CFILES))

#保存文件对象,也就是用在编译时的目标文件名，
#将SFILENDIR下的文件名 .S-->obj/.o  添加了前缀顺便
SOBJS			:= $(patsubst %, obj/%, $(SFILENDIR:.S=.o))
COBJS			:= $(patsubst %, obj/%, $(CFILENDIR:.c=.o))

#所有目标文件（.o）文件名集合
OBJS			:= $(SOBJS)$(COBJS)


############################变量准备完成

#VPATH,用于指定make找文件时，去对应的目录下面找。不然只会在本文件的目录下找
VPATH			:=	$(SRCDIRS)

#伪目标，用于忽略部分文件名，免得把clean当文件，这样就无法执行clean函数了
.PHONY:clean

#基于目标
$(TARGET).bin : $(OBJS)
#编译.o-->.elf
	$(LD) -Timx6u.lds -o $(TARGET).elf $^
	$(OBJCOPY) -O binary -S $(TARGET).elf $@
	$(OBJDUMP) -D -m arm $(TARGET).elf > $(TARGET).dis

#静态模式，多目标规则定义。前两个视为一体或许好点
#目标 : 目标模式 : ) 目标的依赖模式
#作用： 将.S-->.o-->放到obj下
$(SOBJS) : obj/%.o : %.S
#编译，将依赖级，编译为目标集合，加入相关对应路径
	$(CC) -Wall -nostdlib -c -O2 $(INCLUDE) -o $@ $<

$(COBJS) : obj/%.o : %.c
#编译，将依赖级，编译为目标集合，加入相关对应路径
	$(CC) -Wall -nostdlib -c -O2 $(INCLUDE) -o $@ $<


clean:
	rm -rf $(TARGET).elf $(TARGET).bin $(TARGET).dis ${OBJS}

#特点：先声明路径头，然后继续声明其他。
print:
	@echo INCLUDE = $(INCLUDE)
	@echo SFILES = $(SFILES)
	@echo CFILES = $(CFILES)
	@echo SFILENDIR = $(SFILENDIR)
	@echo CFILENDIR = $(CFILENDIR)
	@echo SOBJS = $(SOBJS)
	@echo COBJS = $(COBJS)
	@echo OBJS = $(OBJS)
```



![image-20241117174937957](assets/image-20241117174937957.png)



重点需要学会变量显示（(*^_^*)）





![image-20241117175654711](assets/image-20241117175654711.png)

同时，可以改targ

添加直接改即可，非常方便



### 蜂鸣器

本质来说，和stm32应该类似，也是控制高低电平

#### 原理图分析

![image-20241121221006044](assets/image-20241121221006044.png)

相反的， PNP型号，相对于高电平要压差，因此使得BEEP低电平即可。



#### 引脚搜索

![image-20241121221338266](assets/image-20241121221338266.png)

内部映射的引脚名称为，SNVS TAMPER1(奇怪的是，这里为什么不是按照引脚编号来记忆。)，显然，这个引脚时复用的。



#### 功能流程

1. 始终默认全部打开。
2. 初始化对应引脚IO，复用为GPIO
3. 设置电器属性
4. 初始化GPIO
5. 控制GPIO输出低电平。





#### 代码移植

将功能代码移植，并且修改工作区

![image-20241121222435311](assets/image-20241121222435311.png)

添加新的驱动文件夹到bsp文件夹下



#### 函数设置与参数查找

首先，时钟肯定是配好了的了。

接着就是要配置引脚复用。思考路径如下

>需要配置的对象Beep---》查找底板图，寻找对应的内部引脚名称---》芯片手册查找引脚名称，找到对应外设开启的复用。

这里的Beep--》Tamper1--》GPIO5_IO01 --gpio5;



找到后，在.c文件中进行初始化配置。时钟我们另外配置了，因此不需要理会。这里我们直接配置引脚信息。这里顺便复习。

[引脚关系分析](#引脚关系分析)



到此，我们就明白，如果想要启动外设， 在我们已经配置好了时钟对象，boot引导头等的情况下，我们还需配置对应的外设寄存器。也就来到了，如何实现的环节。也就是函数配置的问题。

[复用引脚配置](#复用引脚配置)



![image-20241122180957005](assets/image-20241122180957005.png)

到此，我们只是完成了复用的配置。复用后的GPIO的相关功能还没配置。



为什么要配置？？？

[引脚功能配置](#引脚功能配置)



```c
#include "bsp_beep.h"
#include "cc.h"                 //ON OFF
//头文件已经声明了的
void beep_init(void){
    IOMUXC_SetPinMux(IOMUXC_SNVS_SNVS_TAMPER1_GPIO5_IO01, 0); //设定复用 GPIO5_IO01,
    IOMUXC_SetPinConfig(IOMUXC_SNVS_SNVS_TAMPER1_GPIO5_IO01, 0x10B0); //设定引脚功能，也就是输出

    //GPIO初始化
    GPIO5->GDIR |= (1<<1);  // __1?  设置GPIO5的IO01位输出（默认0，输入） director 导向寄存器
    GPIO5->DR &= ~(1<<1); // __0? 默认关闭，这样设置是为了不影响别的io      data 数据寄存器


}

void beep_switch(int status){
    if(status == ON){
        GPIO5->DR &= ~(1<<1);  // 点
    }
    else if(status == OFF){
        GPIO5->DR |= (1<<1); // 
    }

}


//--------------------------------------
#ifndef __BSP_BEEP_H_
#define __BSP_BEEP_H_

#include "fsl_common.h"
#include "fsl_iomuxc.h"
#include "MCIMX6Y2.h"
void beep_init(void);
void beep_switch(int status);

#endif // !__BSP_BEEP_H_

```







# 文档分析

## 引脚关系与分析

在查询到外设关联的引脚后（这种一般是出现在用别人的板子的时候发生的。）



我们大概需要搞定这几件事情。

1. 得到外设对应的内部引脚映射名称。这个映射名称对应的时芯片的内部的唯一识别名
2. 基于映射名称，我们可以通过翻看芯片手册，找到该引脚可以实现的功能与配置方式。

现在的问题是

1. 这些配置到底在干什么
2. 怎么配置



### 引脚配置到底在干什么

首先，需要打开引脚目录。

![image-20241122170640588](assets/image-20241122170640588.png)

![image-20241122170653415](assets/image-20241122170653415.png)

在这里，我们可以找到引脚描述，看不到可以直接看图。

![image-20241122170804192](assets/image-20241122170804192.png)

我们发下，这些名称模式，其实就是我们的要配置的寄存器，因此我们所谓的要配置引脚。就是大同这条引脚到内部的，寄存器的路径。



从图中我们可以看出，我们要内部指令发送到引脚端口（只是发送），至少在路径上需要配置两个寄存器。

分别是 IOMUXC_SW_MUX_CTRL_<PAD>[MUX_MODE]  |SW_PAD_CTL  



查找寄存器表

![image-20241122171327639](assets/image-20241122171327639.png)

发现我们要配置的复用引脚的寄存器，其实就是我们前面层次图中的

![image-20241122171355121](assets/image-20241122171355121.png)

结构，因此这样，我们就理解了，原来区别于STM32的复用，IMX6ULL的复用是通过硬件直接的在没用引脚上面单开一个寄存器来配置的。

显然，我们只配置了一个，还需要另一个寄存器。

![image-20241122171523322](assets/image-20241122171523322.png)

显然这个是用于配置我们的末端的引脚状态的。也就理解了为什么叫他配置电器属性了。



# SDK函数分析

## 复用引脚配置

### 直接输出电平的外设

![image-20241122173417164](assets/image-20241122173417164.png)

以Beep位例子，在引脚的时候，首先我们需要配置两个函数。注意，这两个函数配置的是同一个寄存器。即为对应外设对应的内部映射引脚名称的对应寄存器。具体看上面的一脚关系分析。

![image-20241122173550704](assets/image-20241122173550704.png)

也就是在配置这里。

而我们通过两个函数来配置，原因是这样的，我们可以打开宏定义对象![image-20241122173637230](assets/image-20241122173637230.png)

显然，同样的宏定义对应同样的寄存器。找到映射寄存器。

![image-20241122173719171](assets/image-20241122173719171.png)

发现，宏定义的寄存器这里，其实位置是对应的。 



其实就是分配设置了两个寄存器嘛。





SetPinMux用于设置复用

![image-20241122174035134](assets/image-20241122174035134.png)

其只取用了地址与与使能模式。对应的是

![image-20241122174120218](assets/image-20241122174120218.png)



另一个函数用于，在这个引脚被配置位GPIO后，配置该复用的配置值（简单来说就是功能配置）。

![image-20241122174313665](assets/image-20241122174313665.png)

![image-20241122180114300](assets/image-20241122180114300.png)

而对应的寄存器其实就是我们手册的和图像的最末端

![image-20241122180217322](assets/image-20241122180217322.png)

至于要配什么值，就具体看你的需求了



## 引脚功能配置

主要是解决，为什么在设置完复用后，还需要配置功能的问题。如下图



![image-20241122181109810](assets/image-20241122181109810.png)

我们发现，其实在Mux-》Pad间，其实还有这其他旁支电路。也就是这里



![image-20241122181353739](assets/image-20241122181353739.png)

配置完了复用寄存器，复用引脚的电器属性后，本质只是让==复用对象的旁支电路==可以开始干活了而已。因此还需要对旁支电路进行功能配置。



例如GPIO， 需用通过调用对应GPIOx的旁支电路的功能寄存器GDIR与DR来实现输出和数据指定
